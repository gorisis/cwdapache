# SVN_CHECK_CONFIG ([MAJOR-VERSION], [MINOR-VERSION])
# ----------------------------------------------------------
#      Issa Gorissen <flop.m@usa.net>   20140704
#
# Checks for subversion.
#
# This macro defines SVN_INCLUDE.

AC_DEFUN([SVN_CHECK_CONFIG],[
  
  AC_MSG_CHECKING(for Subversion headers)
  AC_ARG_WITH(svn,
  	AS_HELP_STRING([--with-svn=PREFIX], [Subversion (version 1.8.0 or higher) headers looked for in PREFIX/include]),
  	[
  	  if test "$withval" = "no"; then
  	    AC_MSG_ERROR([Subversion is required])
  	  elif test "$withval" != "yes"; then
  	    SVN_PREFIX="$withval"
  	  fi
  	])
  
  if test -z "$SVN_PREFIX"; then
    for i in /usr/local /usr ; do
      if test -f "$i/include/subversion-1/svn_version.h"; then
        SVN_PREFIX="$i"
      	break
      fi
    done
  fi
  
  if test -n "$SVN_PREFIX"; then
    SVN_INCLUDE="$SVN_PREFIX/include/subversion-1"
    if test -r "$SVN_INCLUDE/svn_version.h"; then
      AC_MSG_RESULT(found at $SVN_INCLUDE)
      
      AC_MSG_CHECKING([svn version])
      AC_EGREP_CPP(VERSION_OKAY,
      [
#include "$SVN_INCLUDE/svn_version.h"
#if (($1) < SVN_VER_MAJOR || (($1) == SVN_VER_MAJOR && ($2) <= SVN_VER_MINOR))
VERSION_OKAY
#endif
      ], [AC_MSG_RESULT([recent enough])], [AC_MSG_ERROR([Subversion version is older than 1.8.0])])
    else
      AC_MSG_ERROR(Unable to locate $SVN_INCLUDE/svn_version.h)
      SVN_INCLUDE=""
    fi
  fi
  
  AC_SUBST(SVN_INCLUDE)
])