#ifndef MOD_AUTHZ_CROWD_UTIL_H
#define MOD_AUTHZ_CROWD_UTIL_H

#include <stdbool.h>
#include <httpd.h>
#include <http_log.h>
#include <ap_socache.h>
#include <apr_global_mutex.h>


typedef struct authnz_crowd_srv_config_t
{
    apr_time_t cache_max_age;
    bool cache_max_age_set;
    apr_time_t cache_cookie_config_max_age;
    bool cache_cookie_config_max_age_set;
    bool crowd_ssl_verify_peer;
    bool crowd_ssl_verify_peer_set;

    ap_socache_provider_t *authcacheprovider;
    ap_socache_provider_t *sessioncacheprovider;
    ap_socache_provider_t *groupscacheprovider;
    ap_socache_provider_t *cookieconfigcacheprovider;

    ap_socache_instance_t *authcache;
    ap_socache_instance_t *sessioncache;
    ap_socache_instance_t *groupscache;
    ap_socache_instance_t *cookieconfigcache;
} authnz_crowd_srv_config;


void *log_ralloc(const request_rec *r, void *alloc);

void *log_palloc(apr_pool_t *pool, void *alloc);


#endif
