#ifndef MOD_AUTHNZ_CROWD_H
#define MOD_AUTHNZ_CROWD_H

/* Standard headers */
#include <stdbool.h>
#include <string.h>

/* Apache Portable Runtime includes */
#include <apr_strings.h>
#include <apr_xlate.h>

/* Apache httpd includes */
#include <ap_provider.h>
#include <apr_hooks.h>
#include <httpd.h>
#include <http_core.h>
#include <http_config.h>
#include <http_log.h>
#include <http_request.h>
#include <mod_auth.h>
#include <apr_optional.h>
#include <apr_tables.h>

#include "crowd_client.h"


#define strcEQ(s1,s2)    (strcasecmp(s1,s2)    == 0)
#define CACHE_MAX_AGE		300
#define CACHE_COOKIE_CONFIG_MAX_AGE	3600

APR_DECLARE_OPTIONAL_FN(int, ssl_is_https, (conn_rec *));
static APR_OPTIONAL_FN_TYPE(ssl_is_https) *crowd_is_https = NULL;

apr_array_header_t *authnz_crowd_user_groups(const char *username, request_rec *r);

#endif
