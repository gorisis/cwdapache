#include "mod_authnz_crowd.h"

module AP_MODULE_DECLARE_DATA authnz_crowd_module;

typedef struct
{
    const char *crowd_timeout_string;
    apr_array_header_t *basic_auth_xlates; /* Character translations to be used when decoding Basic authentication
	 credentials. */
    crowd_config *crowd_config;
    bool accept_sso;
    bool accept_sso_set;
    bool create_sso;
    bool create_sso_set;
    bool set_http_only;
    bool set_http_only_set;
    bool ssl_verify_peer_set;

} authnz_crowd_dir_config;

typedef struct
{
    request_rec *r;
    authnz_crowd_dir_config *config;
    char *cookie_name;
    size_t cookie_name_len;
    char *token;
} check_for_cookie_data_t;

static apr_array_header_t *dir_configs = NULL;

apr_global_mutex_t *authmutex = NULL;
apr_global_mutex_t *sessionmutex = NULL;
apr_global_mutex_t *groupsmutex = NULL;
apr_global_mutex_t *cookieconfigmutex = NULL;


/*
 * See top of file for declaration of crowd_is_https variable.
 * You must find https via this method because the mod_ssl
 * hook that sets the environment variable is not run until
 * after the check_user_id hooks.  The old method worked SOMETIMES
 * because Apache will re-use the same subprocess for pipelined
 * requests.
 */

static bool is_https(request_rec *r)
{
    if (!crowd_is_https)
        crowd_is_https = APR_RETRIEVE_OPTIONAL_FN(ssl_is_https);
    int https = 0;
    if (crowd_is_https && crowd_is_https(r->connection))
        https = 1;
    return (bool) https;
}


static authnz_crowd_dir_config *get_config(request_rec *r)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) ap_get_module_config(r->per_dir_config,
                                      &authnz_crowd_module);
    if (config == NULL)
    {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Configuration not found.");
    }
    return config;
}

static unsigned int parse_number(const char *string,
                                 const char *name,
                                 unsigned int min,
                                 unsigned int max,
                                 unsigned int default_value,
                                 server_rec *s)
{
    if (string == NULL)
    {
        return default_value;
    }
    apr_int64_t value = apr_atoi64(string);
    if (errno != 0 || value < 0 || (apr_uint64_t) value > max || (apr_uint64_t) value < min)
    {
        ap_log_error(APLOG_MARK, APLOG_EMERG, errno, s, "Could not parse %s: '%s'", name, string);
        exit(1);
    }
    return (unsigned int) value;
}




/*****************************************************************************
 *
 * Module directory config structure creation
 *
 *****************************************************************************/

/** Function to allow all modules to create per directory configuration
 *  structures.
 *  @param p The pool to use for all allocations.
 *  @param dir The directory currently being processed.
 *  @return The per-directory structure created
 */
static void *create_dir_config(apr_pool_t *p, char *dir)
{
    if (dir == NULL)
    {
        return NULL; // do not create a config object when not in a directory context
    }
    ap_log_perror(APLOG_MARK, APLOG_DEBUG, 0, p, "Creating Crowd config for '%s'", dir);
    authnz_crowd_dir_config *dir_config = log_palloc(p, apr_pcalloc(p, sizeof(authnz_crowd_dir_config)));
    if (dir_config == NULL)
    {
        exit(1);
    }
    dir_config->accept_sso = true;
    dir_config->create_sso = true;
    dir_config->set_http_only = true;
    dir_config->crowd_config = crowd_create_config(p);
    if (dir_config->crowd_config == NULL)
    {
        exit(1);
    }
    dir_config->crowd_config->crowd_ssl_verify_peer = true;
    dir_config->basic_auth_xlates = log_palloc(p, apr_array_make(p, 0, sizeof(apr_xlate_t *)));
    if (dir_config->basic_auth_xlates == NULL)
    {
        exit(1);
    }

    // Add new config to list of this module's per-directory configs, for checking during the post-config phase.
    if (dir_configs == NULL)
    {
        dir_configs = log_palloc(p, apr_array_make(p, 0, sizeof(authnz_crowd_dir_config *)));
        if (dir_configs == NULL)
        {
            exit(1);
        }
    }
    APR_ARRAY_PUSH(dir_configs, authnz_crowd_dir_config *) = dir_config;

    return dir_config;
}



/*****************************************************************************
 *
 * Module server config structure creation
 *
 *****************************************************************************/

/**
 * Handles server configuration
 *
 *  @param p The pool to use for all allocations.
 *  @param s The server being processed.
 */
static void *create_srv_config(apr_pool_t *p, server_rec *s)
{
    authnz_crowd_srv_config *srv_config = log_palloc(p, apr_pcalloc(p, sizeof(authnz_crowd_srv_config)));

    srv_config->cache_max_age = apr_time_from_sec(CACHE_MAX_AGE);
    srv_config->cache_max_age_set = false;

    srv_config->cache_cookie_config_max_age = apr_time_from_sec(CACHE_COOKIE_CONFIG_MAX_AGE);
    srv_config->cache_cookie_config_max_age_set = false;

    srv_config->crowd_ssl_verify_peer = true;
    srv_config->crowd_ssl_verify_peer_set = false;

    return srv_config;
}

static void *merge_srv_config(apr_pool_t *p, void *basev, void *newv)
{
    authnz_crowd_srv_config *rv = log_palloc(p, apr_pcalloc(p, sizeof(authnz_crowd_srv_config)));
    authnz_crowd_srv_config *base = (authnz_crowd_srv_config *) basev;
    authnz_crowd_srv_config *new = (authnz_crowd_srv_config *) newv;

    rv->cache_max_age = (new->cache_max_age_set) ? new->cache_max_age : base->cache_max_age;
    rv->cache_cookie_config_max_age = (new->cache_cookie_config_max_age_set) ? new->cache_cookie_config_max_age : base->cache_cookie_config_max_age;
    rv->crowd_ssl_verify_peer = (new->crowd_ssl_verify_peer_set) ? new->crowd_ssl_verify_peer : base->crowd_ssl_verify_peer;

    rv->authcache = 		(new->authcache) ? 			new->authcache 			: base->authcache;
    rv->cookieconfigcache = (new->cookieconfigcache) ? 	new->cookieconfigcache 	: base->cookieconfigcache;
    rv->sessioncache = 		(new->sessioncache) ? 		new->sessioncache 		: base->sessioncache;
    rv->groupscache = 		(new->groupscache) ? 		new->groupscache 		: base->groupscache;

    rv->authcacheprovider = 		(new->authcacheprovider) ? 			new->authcacheprovider 			: base->authcacheprovider;
    rv->cookieconfigcacheprovider = (new->cookieconfigcacheprovider) ? 	new->cookieconfigcacheprovider 	: base->cookieconfigcacheprovider;
    rv->sessioncacheprovider = 		(new->sessioncacheprovider) ? 		new->sessioncacheprovider 		: base->sessioncacheprovider;
    rv->groupscacheprovider = 		(new->groupscacheprovider) ? 		new->groupscacheprovider 		: base->groupscacheprovider;

    return rv;
}



/*****************************************************************************
 *
 * Module parameters handling functions
 *
 *****************************************************************************/

static const char *set_once_error(const cmd_parms *parms)
{
    const char *error = log_palloc(parms->temp_pool,
                                   apr_psprintf(parms->temp_pool, "%s specified multiple times", parms->cmd->name));
    if (error == NULL)
    {
        error = "Out of memory";
    }
    return error;
}

static const char *set_once(const cmd_parms *parms, const char **location, const char *w)
{
    if (*location != NULL)
    {
        return set_once_error(parms);
    }
    *location = log_palloc(parms->temp_pool, apr_pstrdup(parms->pool, w));
    if (*location == NULL)
    {
        return "Out of memory";
    }
    return NULL;
}

static const char *set_flag_once(const cmd_parms *parms, bool *location,
                                 bool *set_location, int on)
{
    if (*set_location)
    {
        return set_once_error(parms);
    }
    *location = on;
    *set_location = true;
    return NULL;
}

static const char *set_crowd_app_name(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_app_name), w);
}

static const char *set_crowd_app_password(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_app_password), w);
}

static const char *add_basic_auth_conversion(const char *encoding,
        authnz_crowd_dir_config *config,
        apr_pool_t *pconf,
        apr_pool_t *ptemp)
{
    apr_xlate_t *conversion;
    if (apr_xlate_open(&conversion, "UTF-8", encoding, pconf) != APR_SUCCESS)
    {
        const char *error = log_palloc(ptemp, apr_psprintf(ptemp, "Encoding not supported: '%s'", encoding));
        if (error == NULL)
        {
            error = "Out of memory";
        }
        return error;
    }
    APR_ARRAY_PUSH(config->basic_auth_xlates, apr_xlate_t *) = conversion;
    return NULL;
}

static const char *set_crowd_basic_auth_encoding(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return add_basic_auth_conversion(w, config, parms->pool, parms->temp_pool);
}

static const char *set_crowd_timeout(cmd_parms *parms, void *mconfig, const char *w)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_timeout_string), w);
}

static const char *set_crowd_cert_path(cmd_parms *parms, void *mconfig, const char *w)
{
    // Ignore empty URLs.  Will be reported as a missing parameter.
    if (*w == '\0')
    {
        return NULL;
    }

    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_cert_path), w);
}

static const char *set_crowd_url(cmd_parms *parms, void *mconfig, const char *w)
{
    // Ignore empty URLs.  Will be reported as a missing parameter.
    if (*w == '\0')
    {
        return NULL;
    }
    // Add a trailing slash if one does not already exist.
    if (w[strlen(w) - 1] != '/')
    {
        w = log_palloc(parms->temp_pool, apr_pstrcat(parms->temp_pool, w, "/", NULL));
        if (w == NULL)
        {
            return "Out of memory";
        }
    }

    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_once(parms, &(config->crowd_config->crowd_url), w);
}

static const char *set_crowd_cache_max_age(cmd_parms *parms, void *dummy, const char *w)
{
    const char *err;

    if ((err = ap_check_cmd_context(parms, GLOBAL_ONLY)))
    {
        return err;
    }

    authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
    srv_config->cache_max_age = apr_time_from_sec(parse_number(w, "CrowdCacheMaxAge", 0, UINT_MAX, CACHE_MAX_AGE, parms->server));
    return NULL;
}


static const char *set_crowd_cache_cookie_config_max_age(cmd_parms *parms, void *dummy, const char *w)
{
    const char *err;

    if ((err = ap_check_cmd_context(parms, GLOBAL_ONLY)))
    {
        return err;
    }

    authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
    srv_config->cache_cookie_config_max_age = apr_time_from_sec(parse_number(w, "CrowdCacheCookieConfigMaxAge", 0, UINT_MAX, CACHE_COOKIE_CONFIG_MAX_AGE, parms->server));
    return NULL;
}


static const char *set_crowd_socache_auth_path(cmd_parms *parms, void *dummy, const char *w)
{
    const char *err, *sep, *name;

    if ((err = ap_check_cmd_context(parms, GLOBAL_ONLY)))
    {
        return err;
    }

    sep = ap_strchr_c(w, ':');
    if (sep)
    {
        name = apr_pstrmemdup(parms->pool, w, sep - w);
        sep++;
    }
    else
    {
        name = w;
    }

    /* Find the provider of given name. */
    authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
    srv_config->authcacheprovider = ap_lookup_provider(AP_SOCACHE_PROVIDER_GROUP,
                                    name,
                                    AP_SOCACHE_PROVIDER_VERSION);
    if (srv_config->authcacheprovider)
    {
        /* Cache found; create it, passing anything beyond the colon. */
        err = srv_config->authcacheprovider->create(&srv_config->authcache, sep, parms->temp_pool, parms->pool);
    }
    else
    {
        err = apr_psprintf(parms->pool, "'%s' session cache not supported. "
                           "Maybe you need to load the "
                           "appropriate socache module (mod_socache_%s?).",
                           name, name);
    }

    if (err)
    {
        return apr_psprintf(parms->pool, "%s: %s", parms->cmd->name, err);
    }

    return NULL;
}

static const char *set_crowd_socache_cookieconfig_path(cmd_parms *parms, void *dummy, const char *w)
{
    const char *err, *sep, *name;

    if ((err = ap_check_cmd_context(parms, GLOBAL_ONLY)))
    {
        return err;
    }

    sep = ap_strchr_c(w, ':');
    if (sep)
    {
        name = apr_pstrmemdup(parms->pool, w, sep - w);
        sep++;
    }
    else
    {
        name = w;
    }

    authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
    srv_config->cookieconfigcacheprovider = ap_lookup_provider(AP_SOCACHE_PROVIDER_GROUP,
                                            name,
                                            AP_SOCACHE_PROVIDER_VERSION);
    if (srv_config->cookieconfigcacheprovider)
    {
        /* Cache found; create it, passing anything beyond the colon. */
        err = srv_config->cookieconfigcacheprovider->create(&srv_config->cookieconfigcache, sep, parms->temp_pool, parms->pool);
    }
    else
    {
        err = apr_psprintf(parms->pool, "'%s' session cache not supported. "
                           "Maybe you need to load the "
                           "appropriate socache module (mod_socache_%s?).",
                           name, name);
    }

    if (err)
    {
        return apr_psprintf(parms->pool, "%s: %s", parms->cmd->name, err);
    }

    return NULL;
}

static const char *set_crowd_socache_session_path(cmd_parms *parms, void *dummy, const char *w)
{
    const char *err, *sep, *name;

    if ((err = ap_check_cmd_context(parms, GLOBAL_ONLY)))
    {
        return err;
    }

    sep = ap_strchr_c(w, ':');
    if (sep)
    {
        name = apr_pstrmemdup(parms->pool, w, sep - w);
        sep++;
    }
    else
    {
        name = w;
    }

    authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
    srv_config->sessioncacheprovider = ap_lookup_provider(AP_SOCACHE_PROVIDER_GROUP,
                                       name,
                                       AP_SOCACHE_PROVIDER_VERSION);
    if (srv_config->sessioncacheprovider)
    {
        /* Cache found; create it, passing anything beyond the colon. */
        err = srv_config->sessioncacheprovider->create(&srv_config->sessioncache, sep, parms->temp_pool, parms->pool);
    }
    else
    {
        err = apr_psprintf(parms->pool, "'%s' session cache not supported. "
                           "Maybe you need to load the "
                           "appropriate socache module (mod_socache_%s?).",
                           name, name);
    }

    if (err)
    {
        return apr_psprintf(parms->pool, "%s: %s", parms->cmd->name, err);
    }

    return NULL;
}

static const char *set_crowd_socache_groups_path(cmd_parms *parms, void *dummy, const char *w)
{
    const char *err, *sep, *name;

    if ((err = ap_check_cmd_context(parms, GLOBAL_ONLY)))
    {
        return err;
    }

    sep = ap_strchr_c(w, ':');
    if (sep)
    {
        name = apr_pstrmemdup(parms->pool, w, sep - w);
        sep++;
    }
    else
    {
        name = w;
    }

    authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
    srv_config->groupscacheprovider = ap_lookup_provider(AP_SOCACHE_PROVIDER_GROUP,
                                      name,
                                      AP_SOCACHE_PROVIDER_VERSION);
    if (srv_config->groupscacheprovider)
    {
        /* Cache found; create it, passing anything beyond the colon. */
        err = srv_config->groupscacheprovider->create(&srv_config->groupscache, sep, parms->temp_pool, parms->pool);
    }
    else
    {
        err = apr_psprintf(parms->pool, "'%s' session cache not supported. "
                           "Maybe you need to load the "
                           "appropriate socache module (mod_socache_%s?).",
                           name, name);
    }

    if (err)
    {
        return apr_psprintf(parms->pool, "%s: %s", parms->cmd->name, err);
    }

    return NULL;
}

static const char *set_crowd_accept_sso(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->accept_sso), &(config->accept_sso_set), on);
}

static const char *set_crowd_create_sso(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->create_sso), &(config->create_sso_set), on);
}

static const char *set_crowd_set_http_only(cmd_parms *parms, void *mconfig, int on)
{
    authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
    return set_flag_once(parms, &(config->set_http_only), &(config->set_http_only_set), on);
}

static const char *set_crowd_ssl_verify_peer(cmd_parms *parms, void *mconfig, int on)
{
    if (parms->path)
    {
        authnz_crowd_dir_config *config = (authnz_crowd_dir_config *) mconfig;
        return set_flag_once(parms, &(config->crowd_config->crowd_ssl_verify_peer), &(config->ssl_verify_peer_set), on);
    }
    else
    {
        authnz_crowd_srv_config *srv_config = ap_get_module_config(parms->server->module_config, &authnz_crowd_module);
        return set_flag_once(parms, &(srv_config->crowd_ssl_verify_peer), &(srv_config->crowd_ssl_verify_peer_set), on);
    }
}

static const command_rec commands[] =
{
    /* Directory settings */
    AP_INIT_TAKE1("CrowdAppName", set_crowd_app_name, NULL, OR_AUTHCFG, "The name of this application, as configured in Crowd"),
    AP_INIT_TAKE1("CrowdAppPassword", set_crowd_app_password, NULL, OR_AUTHCFG, "The password of this application, as configured in Crowd"),
    AP_INIT_TAKE1("CrowdTimeout", set_crowd_timeout, NULL, OR_AUTHCFG, "The maximum length of time, in seconds, to wait for a response from Crowd (default or 0 = no timeout)"),
    AP_INIT_TAKE1("CrowdURL", set_crowd_url, NULL, OR_AUTHCFG, "The base URL of the Crowd server"),
    AP_INIT_TAKE1("CrowdCertPath", set_crowd_cert_path, NULL, OR_AUTHCFG, "The path to the SSL certificate file to supply to curl for Crowd over SSL"),

    AP_INIT_ITERATE("CrowdBasicAuthEncoding", set_crowd_basic_auth_encoding, NULL, OR_AUTHCFG, "The list of character encodings that will be used to interpret Basic authentication credentials " "(default is ISO-8859-1 only"),

    AP_INIT_FLAG("CrowdAcceptSSO", set_crowd_accept_sso, NULL, OR_AUTHCFG, "'On' if single-sign on cookies should be accepted; 'Off' otherwise (default = On)"),
    AP_INIT_FLAG("CrowdCreateSSO", set_crowd_create_sso, NULL, OR_AUTHCFG, "'On' if single-sign on cookies should be created; 'Off' otherwise (default = On)"),
    AP_INIT_FLAG("CrowdSetHttpOnly", set_crowd_set_http_only, NULL, OR_AUTHCFG, "'On' if single-sign on cookies should be created as HttpOnly; 'Off' otherwise (default = On)"),

    /* Can be set globally or inside Directory/Location */
    AP_INIT_FLAG("CrowdSSLVerifyPeer", set_crowd_ssl_verify_peer, NULL, OR_AUTHCFG | RSRC_CONF, "'On' if SSL certificate validation should occur when connecting to Crowd; 'Off' otherwise (default = On)"),

    /* Global settings */
    AP_INIT_TAKE1("CrowdCacheMaxAge", set_crowd_cache_max_age, NULL, RSRC_CONF, "The maximum length of time that successful results from Crowd can be cached, in seconds (default = 300 seconds)"),
    AP_INIT_TAKE1("CrowdCacheCookieConfigMaxAge", set_crowd_cache_cookie_config_max_age, NULL, RSRC_CONF, "The maximum length of time that successful results from Crowd can be cached concerning Crowd cookie config, in seconds (default = 3600 seconds)"),
    AP_INIT_TAKE1("CrowdSoCacheAuthPath", set_crowd_socache_auth_path, NULL, RSRC_CONF, "Path to the DBM SoCache instance for authentication results (dbm:/path/to/datafile)"),
    AP_INIT_TAKE1("CrowdSoCacheCookieConfigPath", set_crowd_socache_cookieconfig_path, NULL, RSRC_CONF, "Path to the DBM SoCache instance for crowd cookie config result (dbm:/path/to/datafile)"),
    AP_INIT_TAKE1("CrowdSoCacheSessionPath", set_crowd_socache_session_path, NULL, RSRC_CONF, "Path to the DBM SoCache instance for session results (dbm:/path/to/datafile)"),
    AP_INIT_TAKE1("CrowdSoCacheGroupsPath", set_crowd_socache_groups_path, NULL, RSRC_CONF, "Path to the DBM SoCache instance for user groups results (dbm:/path/to/datafile)"),
    { 0 }
};





/*****************************************************************************
 *
 * Hooks
 *
 *****************************************************************************/


/* Post Config Hook */

apr_status_t cleanup(void *data)
{
    server_rec *s = (server_rec *) data;
    authnz_crowd_srv_config *srv_config = ap_get_module_config(s->module_config, &authnz_crowd_module);

    if (srv_config->authcacheprovider)
    {
        srv_config->authcacheprovider->destroy(srv_config->authcache, s);
    }
    if (srv_config->cookieconfigcacheprovider)
    {
        srv_config->cookieconfigcacheprovider->destroy(srv_config->cookieconfigcache, s);
    }
    if (srv_config->groupscacheprovider)
    {
        srv_config->groupscacheprovider->destroy(srv_config->groupscache, s);
    }
    if (srv_config->sessioncacheprovider)
    {
        srv_config->sessioncacheprovider->destroy(srv_config->sessioncache, s);
    }

    if (authmutex)
    {
        apr_global_mutex_destroy(authmutex);
    }
    if (cookieconfigmutex)
    {
        apr_global_mutex_destroy(cookieconfigmutex);
    }
    if (sessionmutex)
    {
        apr_global_mutex_destroy(sessionmutex);
    }
    if (groupsmutex)
    {
        apr_global_mutex_destroy(groupsmutex);
    }

    return APR_SUCCESS;
}

/* Called after configuration is set, to finalise it. */
static int post_config(apr_pool_t *pconf, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s)
{
    /*
     * Do nothing if we are not creating the final configuration.
     * The parent process gets initialized a couple of times as the
     * server starts up.
     */
    if (ap_state_query(AP_SQ_MAIN_STATE) == AP_SQ_MS_CREATE_PRE_CONFIG)
        return APR_SUCCESS;


    // register cleanup function
    apr_pool_pre_cleanup_register(pconf, s, cleanup);



    /* Create the caches, if required. */
    authnz_crowd_srv_config *srv_config = ap_get_module_config(s->module_config, &authnz_crowd_module);
    if (!crowd_cache_create(pconf, s))
    {
        exit(1);
    }


    if (dir_configs != NULL)
    {

        /* Iterate over each directory config */
        authnz_crowd_dir_config **dir_config;
        while ((dir_config = apr_array_pop(dir_configs)) != NULL)
        {
            bool ssl_verify_peer;

            /* If any of the configuration parameters are specified, ensure that all mandatory parameters are
             specified. */
            crowd_config *crowd_config = (*dir_config)->crowd_config;
            if ((crowd_config->crowd_app_name != NULL || crowd_config->crowd_app_password != NULL
                    || crowd_config->crowd_url != NULL)
                    && (crowd_config->crowd_app_name == NULL || crowd_config->crowd_app_password == NULL
                        || crowd_config->crowd_url == NULL))
            {
                ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s,
                             "Missing CrowdAppName, CrowdAppPassword or CrowdURL for a directory.");
                exit(1);
            }

            /* If CrowdSSLVerifyPeer is not set at Dir level, take Server setting */
            if (!(*dir_config)->ssl_verify_peer_set)
            {
                crowd_config->crowd_ssl_verify_peer = srv_config->crowd_ssl_verify_peer;
            }
            if ((crowd_config->crowd_app_name != NULL || crowd_config->crowd_app_password != NULL
                    || crowd_config->crowd_url != NULL)
                    && (crowd_config->crowd_ssl_verify_peer && crowd_config->crowd_cert_path == NULL))
            {
                ap_log_error(APLOG_MARK, APLOG_NOTICE, 0, s,
                             "CrowdSSLVerifyPeer is On but CrowdCertPath is unspecified.");
            }

            /* Parse the timeout parameter, if specified */
            crowd_config->crowd_timeout = parse_number((*dir_config)->crowd_timeout_string, "CrowdTimeout", 0,
                                          UINT_MAX, 0, s);

            /* If no basic auth character encodings are specified, setup ISO-8859-1. */
            if (apr_is_empty_array((*dir_config)->basic_auth_xlates))
            {
                const char *error = add_basic_auth_conversion("ISO-8859-1", *dir_config, pconf, ptemp);
                if (error != NULL)
                {
                    ap_log_error(APLOG_MARK, APLOG_EMERG, 0, s,
                                 "Could not configure default Basic Authentication translation.  %s", error);
                    exit(1);
                }
            }

        }
    }
    return OK;
}



/* Check Access Hook */

static int check_for_cookie(void *rec, const char *key, const char *value)
{
    if (strcEQ("Cookie", key))
    {
        check_for_cookie_data_t *data = rec;
        if (data->cookie_name == NULL)
        {
            crowd_cookie_config_t *cookie_config = crowd_get_cookie_config(data->r, data->config->crowd_config);
            if (cookie_config == NULL || cookie_config->cookie_name == NULL
                    || (cookie_config->secure && !is_https(data->r)))
            {
                return 0;
            }
            data->cookie_name = log_ralloc(data->r, apr_pstrcat(data->r->pool, cookie_config->cookie_name, "=",
                                           NULL));
            if (data->cookie_name == NULL)
            {
                return 0;
            }
            data->cookie_name_len = strlen(data->cookie_name);
        }
        char *cookies = log_ralloc(data->r, apr_pstrdup(data->r->pool, value));
        if (cookies == NULL)
        {
            return 0;
        }
        apr_collapse_spaces(cookies, cookies);
        char *last;
        char *cookie = apr_strtok(cookies, ";,", &last);
        while (cookie != NULL)
        {
            if (strncasecmp(cookie, data->cookie_name, data->cookie_name_len) == 0)
            {
                data->token = log_ralloc(data->r, apr_pstrdup(data->r->pool, cookie + data->cookie_name_len));
                return 0;
            }
            cookie = apr_strtok(NULL, ";,", &last);
        }
    }
    return 1;
}

static int check_access(request_rec *r)
{
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "check_user_id");
    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL || !(config->accept_sso))
    {
        return DECLINED;
    }
    check_for_cookie_data_t data =
    { .r = r, .config = config };
    apr_table_do(check_for_cookie, &data, r->headers_in, NULL);

    if (data.token != NULL && crowd_validate_session(r, config->crowd_config, data.token, &r->user) == CROWD_AUTHENTICATE_SUCCESS)
    {
        r->ap_auth_type = "Crowd SSO";
        return OK;
    }
    return DECLINED;
}



/* Child Init */

static void child_init(apr_pool_t *p, server_rec *s)
{
    open_global_mutex_for_child_process(p, s);
}



/*****************************************************************************
 *
 * Auth Providers
 *
 *****************************************************************************/

#define XLATE_BUFFER_SIZE 256
static bool xlate_string(apr_xlate_t *xlate, const char *input, char *output)
{
    apr_size_t input_left = strlen(input);
    apr_size_t output_left = XLATE_BUFFER_SIZE;
    if (apr_xlate_conv_buffer(xlate, input, &input_left, output, &output_left) != APR_SUCCESS || input_left != 0
            || apr_xlate_conv_buffer(xlate, NULL, NULL, output, &output_left) != APR_SUCCESS || output_left < 1)
    {
        return false;
    }
    return true;
}

/* Given a username and password, expected to return AUTH_GRANTED if we can validate this user/password combination. */
static authn_status authn_crowd_check_password(request_rec *r, const char *user, const char *password)
{
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "authn_crowd_check_password");

    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL)
    {
        return AUTH_GENERAL_ERROR;
    }

    apr_array_header_t *basic_auth_xlates = config->basic_auth_xlates;
    int i;
    for (i = 0; i < basic_auth_xlates->nelts; i++)
    {
        apr_xlate_t *xlate = APR_ARRAY_IDX(basic_auth_xlates, i, apr_xlate_t *);
        char xlated_user[XLATE_BUFFER_SIZE] = { };
        char xlated_password[XLATE_BUFFER_SIZE] = { };
        if (!xlate_string(xlate, user, xlated_user) || !xlate_string(xlate, password, xlated_password))
        {
            ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Failed to translate basic authentication credentials");
        }
        else
        {
            crowd_authenticate_result result = CROWD_AUTHENTICATE_NOT_ATTEMPTED;
            if (config->create_sso)
            {
                crowd_cookie_config_t *cookie_config = crowd_get_cookie_config(r, config->crowd_config);
                if (cookie_config != NULL && (!cookie_config->secure || is_https(r)))
                {
                    const char *token;
                    result = crowd_create_session(r, config->crowd_config, xlated_user, xlated_password, &token);
                    if (result == CROWD_AUTHENTICATE_SUCCESS && token != NULL)
                    {
                        char *domain = "";
                        bool skip_cookie = false;
                        if (cookie_config->domain != NULL && strlen(cookie_config->domain) > 0 && cookie_config->domain[0] == '.')
                        {
                            size_t domainlen = strlen(cookie_config->domain);
                            size_t hostlen = strlen(r->hostname);
                            if (hostlen > domainlen
                                    && strcmp(cookie_config->domain, r->hostname + hostlen - domainlen) == 0)
                            {
                                domain = apr_psprintf(r->pool, ";Domain=%s", cookie_config->domain);
                            }
                            else
                            {
                                skip_cookie = true;
                                ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
                                              "Cannot set a SSO cookie for hostname %s while Crowd expects cookie domain %s", r->hostname, cookie_config->domain);
                            }
                        }

                        if (!skip_cookie)
                        {
                            char *cookie = log_ralloc(r,
                                                      apr_psprintf(r->pool, "%s=%s%s%s%s;Version=1;Path=/", cookie_config->cookie_name, token,
                                                                   domain, cookie_config->secure ? ";Secure" : "",
                                                                   config->set_http_only ? ";HttpOnly" : ""));
                            if (cookie != NULL)
                            {
                                apr_table_add(r->err_headers_out, "Set-Cookie", cookie);
                            }
                        }
                    }
                }
            }
            if (result == CROWD_AUTHENTICATE_NOT_ATTEMPTED)
            {
                result = crowd_authenticate(r, config->crowd_config, xlated_user, xlated_password);
            }
            switch (result)
            {
            case CROWD_AUTHENTICATE_SUCCESS:
                ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Crowd authenticated '%s'.", xlated_user);
                return AUTH_GRANTED;
            case CROWD_AUTHENTICATE_FAILURE:
                break;
            default:
                ap_log_rerror(APLOG_MARK, APLOG_CRIT, 0, r, "Crowd authentication failed due to system exception");
                return AUTH_GENERAL_ERROR;
            }
        }
    }

    return AUTH_DENIED;
}

static const authn_provider authn_crowd_provider =
{
    &authn_crowd_check_password, /* Callback for HTTP Basic authentication */
    NULL /* Callback for HTTP Digest authentication */
};

/*****************************************************************************/

static authz_status crowdgroup_check_authorization(request_rec *r,
        const char *require_args,
        const void *parsed_require_args)
{
    const char *t;
    char *w;

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "crowdgroup_check_authorization");

    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL)
    {
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    if (!r->user)
    {
        ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Authorisation requested, but no user provided.");
        return AUTHZ_DENIED_NO_USER;
    }

    /* Iterate over requirements */
    t = require_args;
    apr_array_header_t *user_groups = NULL;
    while ((w = ap_getword_white(r->pool, &t)) && w[0])
    {
        /* Fetch groups only if actually needed. */
        if (user_groups == NULL)
        {
            user_groups = crowd_user_groups(r->user, r, config->crowd_config);
            if (user_groups == NULL)
            {
                return AUTHZ_DENIED;
            }
        }
        /* Iterate over the user's groups. */
        int y;
        for (y = 0; y < user_groups->nelts; y++)
        {
            const char *user_group = APR_ARRAY_IDX(user_groups, y, const char *);
            if (strcEQ(user_group, w))
            {
                ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r,
                              "Granted authorisation to '%s' on the basis of membership of '%s'.", r->user, user_group);
                return AUTHZ_GRANTED;
            }
        }
    }

    ap_log_rerror(APLOG_MARK, APLOG_NOTICE, 0, r, "Denied authorisation to '%s'.", r->user);
    return AUTHZ_DENIED;

}

static const authz_provider authz_crowdgroup_provider =
{
    &crowdgroup_check_authorization,
    NULL,
};




/*****************************************************************************
 *
 * Exported functions
 *
 *****************************************************************************/

apr_array_header_t *authnz_crowd_user_groups(const char *username, request_rec *r)
{
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "authnz_crowd_user_groups");
    authnz_crowd_dir_config *config = get_config(r);
    if (config == NULL)
    {
        return NULL;
    }
    return crowd_user_groups(username, r, config->crowd_config);
}





/*****************************************************************************
 *
 * Register hooks
 *
 *****************************************************************************/


static void register_hooks(apr_pool_t *p)
{
    ap_hook_post_config(post_config, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_check_access(check_access, NULL, NULL, APR_HOOK_FIRST, AP_AUTH_INTERNAL_PER_CONF);
    ap_hook_child_init(child_init, NULL, NULL, APR_HOOK_MIDDLE);

    ap_register_auth_provider(p, AUTHN_PROVIDER_GROUP, "crowd", AUTHN_PROVIDER_VERSION, &authn_crowd_provider,
                              AP_AUTH_INTERNAL_PER_CONF);

    ap_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "crowd-group", AUTHZ_PROVIDER_VERSION,
                              &authz_crowdgroup_provider, AP_AUTH_INTERNAL_PER_CONF);
}




/*****************************************************************************
 *
 * Module declaration
 *
 *****************************************************************************/

AP_DECLARE_MODULE(authnz_crowd) =
{
    STANDARD20_MODULE_STUFF,
    create_dir_config, /* create per-directory config structure */
    NULL, /* merge per-directory config structures */
    create_srv_config, /* create per-server config structure */
    merge_srv_config, /* merge per-server config structures */
    commands, /* command apr_table_t */
    register_hooks /* register hooks */
};





/* Library initialisation and termination functions */
/* TODO: Another solution will likely be required for non-GCC platforms, e.g. Windows */

void init() __attribute__ ((constructor));

void init()
{
    crowd_init();
}

void term() __attribute__ ((destructor));

void term()
{
    crowd_cleanup();
}
