#include "cache.h"

apr_status_t cache_get(const request_rec *r, apr_global_mutex_t *mutex, char *cachename,
                       ap_socache_provider_t *provider, ap_socache_instance_t *instance,
                       const unsigned char *key, unsigned char *data, unsigned int *datalen)
{
    apr_status_t rs;

    rs = apr_global_mutex_lock(mutex);
    if (rs != APR_SUCCESS)
    {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, rs, r, "Unable to acquire global mutex");
        return rs;
    }

    rs = provider->retrieve(instance, r->server, key, strlen(key), data, datalen, r->pool);

    apr_global_mutex_unlock(mutex);

    if (APLOG_R_IS_LEVEL(r, APLOG_DEBUG))
    {
        if (APR_SUCCESS != rs)
        {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Cache '%s' miss for '%s'", cachename, key);
        }
        else
        {
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Cache '%s' hit for '%s'", cachename, key);
        }
    }

    return rs;
}

void cache_put(const request_rec *r, apr_global_mutex_t *mutex,
               ap_socache_provider_t *provider, ap_socache_instance_t *instance, apr_time_t ttl,
               const unsigned char *key, unsigned char *data, unsigned int datalen)
{
    apr_status_t rs;

    rs = apr_global_mutex_lock(mutex);
    if (rs != APR_SUCCESS)
    {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, rs, r, "Unable to acquire global mutex");
        return;
    }

    apr_time_t expiry = apr_time_now() + ttl;
    rs = provider->store(instance, r->server, key, strlen(key), expiry, data, datalen, r->pool);

    apr_global_mutex_unlock(mutex);
}
