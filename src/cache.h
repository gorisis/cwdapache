#ifndef CROWD_CACHE_H
#define CROWD_CACHE_H

#include <apr_hash.h>
#include <apr_global_mutex.h>

#include "util.h"


apr_status_t cache_get(const request_rec *r, apr_global_mutex_t *mutex, char *cachename,
                       ap_socache_provider_t *provider, ap_socache_instance_t *instance,
                       const unsigned char *key, unsigned char *data, unsigned int *datalen);

void cache_put(const request_rec *r, apr_global_mutex_t *mutex,
               ap_socache_provider_t *provider, ap_socache_instance_t *instance, apr_time_t ttl,
               const unsigned char *key, unsigned char *data, unsigned int datalen);


#endif
